<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\News;
use Illuminate\Validation\ValidationException;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perpage = 10;
        $count = News::all()->count();
        $paginator = News::paginate($perpage);
        return view('admin.news.index', compact('count', 'paginator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'text' => 'required|max:400',
        ]);

        $result = News::create([
            'title' => $request['title'],
            'text' => $request['text'],
            'author' => auth()->user()->name,
        ]);

        if (!$result) {
            session()->flash('danger', 'Ошибка Создания');
            return back();

        } else {
            session()->flash('success', 'Новость успешно создана');
            return redirect()
                ->route('admin.news.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = News::find($id);
        return view('admin.news.edit', compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'text' => 'required|max:400',
        ]);

        $result = News::find($id);

        $result->title = $request['title'];
        $result->text = $request['text'];
        $save = $result->save();

        if (!$save) {
            session()->flash('danger', 'Ошибка Создания');
            return back();
        } else {
            session()->flash('success', 'Успешно отредактировано');

            return redirect()->route('admin.news.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $del = News::find($id);
        $result = $del->forceDelete();
        if (!$result) {
            session()->flash('danger', 'Ошибка Удаления');
            return back();

        } else {
            session()->flash('success', 'Успешно удалено');
            return redirect()
                ->route('admin.news.index');


        }
    }

}
