<?php

namespace App\Http\Controllers;

use App\News;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $perpage = 10;
        $count = News::all()->count();
        $paginator = News::paginate($perpage);
        return view('welcome', compact('count', 'paginator'));

         }
}
