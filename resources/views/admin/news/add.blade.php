@extends('layouts.app_admin')


@section('content')
    @if (session()->has('success'))
        <p class="alert alert-success">{{session()->get('success')}}</p>
    @endif

    @if (session()->has('warning'))
        <p class="alert alert-warning">{{session()->get('warning')}}</p>
    @endif
    @if (session()->has('danger'))
        <p class="alert alert-danger">{{session()->get('dander')}}</p>
    @endif

    @if ($errors->any())

        @foreach ($errors->all() as $error)
            <script>
                iziToast.error({timeout: 5000, position: 'topCenter',  icon: 'fa fa-exclamation-triangle', title: 'Ошибка.', message: "{{$error}}"});
            </script>
        @endforeach

    @endif





    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <!--для валидации data-toggle="validator"-->
                    <form action="{{route('admin.news.store')}}" method="post" data-toggle="validator">

                        @csrf
                        <div class="box-body">
                            <div class="row">
                                <div class="form-group col-md-3 has-feedback" >
                                    <label for="title">Заголовок
                                    </label>
                                    <input type="text" class="form-control" name="title" placeholder="Введите заголовок " required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group col-md-6 has-feedback" >
                                    <label for="text">Текст новости
                                    </label>
                                    <input type="text" class="form-control" name="text" placeholder="Введите текст новости " required>
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>

                                </div>

                            </div>



                        </div>
                        <div class="box-footer">
                            <input type="hidden" name="id" value="">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </section>
    <!-- /.content -->



@endsection