@extends('layouts.app_admin')

@section('content')
    @if (session()->has('success'))
        <p class="alert alert-success">{{session()->get('success')}}</p>
    @endif

    @if (session()->has('warning'))
        <p class="alert alert-warning">{{session()->get('warning')}}</p>
    @endif
    @if (session()->has('danger'))
        <p class="alert alert-danger">{{session()->get('dander')}}</p>
    @endif

    @if ($errors->any())

        @foreach ($errors->all() as $error)
            <script>
                iziToast.error({timeout: 5000, position: 'topCenter',  icon: 'fa fa-exclamation-triangle', title: 'Ошибка.', message: "{{$error}}"});
            </script>
        @endforeach

    @endif





    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">

                    <form action="{{route('admin.news.update', $edit->id)}}" method="post" data-toggle="validator">
                        @method('PUT')
                        @csrf
                        <div class="box-body">
                            <div class="row">
                            <div class="form-group col-md-3 has-feedback" >
                                <label for="title">Заголовок
                                </label>
                                <input type="text" class="form-control" name="title" placeholder="" value="{{$edit->title}}" required >
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            </div>
                            <div class="form-group col-md-6 has-feedback">
                                    <label for="">Текст</label>
                                    <input type="text" class="form-control" name="text" value="{{$edit->text}}"
                                           placeholder="Введите текст">
                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>



                            </div>
                            <div class="row">





                            </div>



                        </div>
                        <div class="box-footer">
                            <input type="hidden" name="id" value="">
                            <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                        </div>
                    </form>
                </div>
              



            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection
