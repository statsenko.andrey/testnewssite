@extends('layouts.app_admin')

@section('content')
    @if (session()->has('success'))
        <script>
            iziToast.success({timeout: 5000, position: 'topCenter',  icon: 'fa fa-check', title: 'Отлично.', message: "{{session()->get('success')}}"});
        </script>
    @endif

    @if (session()->has('warning'))
        <p class="alert alert-warning">{{session()->get('warning')}}</p>
    @endif
    @if (session()->has('danger'))
        <p class="alert alert-danger">{{session()->get('dander')}}</p>
    @endif

    @if ($errors->any())

        @foreach ($errors->all() as $error)
            <script>
                iziToast.error({timeout: 5000, position: 'topCenter',  icon: 'fa fa-exclamation-triangle', title: 'Ошибка.', message: "{{$error}}"});
            </script>
        @endforeach

    @endif



    <section class="content-header">




    </section>


    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Заголовок</th>
                                    <th>Автор</th>
                                    <th>Дата создания</th>

                                    <th>
                                        <form action="{{route('admin.news.create')}}">
                                        <button type="submit" class="btn btn-primary" style="float: right;">+ Добавить новость</button>
                                    </form>
                                    </th>


                                </tr>
                                </thead>

                                <tbody>
                                @forelse($paginator as $item)

                                    <tr>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->author}}</td>
                                        <td>{{$item->created_at}}</td>



                                        <td>
                                            <a href="{{route('admin.news.edit', $item->id)}}" title="редактировать новость"><i class="btn btn-xs"></i>
                                                <button type="submit" class="btn btn-success btn-xs">редактировать</button>
                                            </a>

                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                            <a class="btn btn-xs">
                                                <form method="post"  action="{{route('admin.news.destroy', $item->id)}}"

                                                      style="float: none">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger btn-xs ">Удалить</button>
                                                </form>
                                            </a>

                                        </td>

                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3" class="text-center"><h2>Нет записей</h2></td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                            <p>{{count($paginator)}} записей из {{$count}} </p>

                            @if ($paginator->total() > $paginator->count())
                                <br>
                                <div class="row justify-content-center">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                {{$paginator->links()}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->

@endsection
